//analysisOperations.js
//$(document).ready(function(){
//    $('[data-toggle="popover"]').popover();
//});
var analyseData;
var fileDir;
function analyseUI(path, format){
fileDir=getInputPath(path);
$.ajax({
        method: "GET",
        dataType: "json",
        async: "true",
		url: location.origin + '/getAppsByFileFormat/?Format='+format,
	    success: function (data, status, jqXHR) {
            loadingDisplay(".rightLoad",false);
            analyseOps(data);
        },
        error: function (jqXHR, status, err) {
            alert("Sorry! Unable to generate Analysis form. Please try again or contact admin.")
	    },
            complete: function (jqXHR, status) {
        }
	});
}

function trimAppDescription(description){
    if (description.includes('%INPUT%')){
        return description.slice(0, description.indexOf('%INPUT%'));
    }else{
        return description
    }
}

function analyseOps(data){
    $(".accord").remove();
    for(var i=0;i<data.length;i++){
        var acco= "<div class='list-group-item accord'>"
            acco+="<div class='container'>"
            acco+="<button type='button' class='btn btn-primary text-white w-100' onclick='analyseForm(\""+data[i].id+"\",\""+data[i].Name+"\")'>"+data[i].Name+"</button>"
            acco+="<button class='btn  w-100 ' data-toggle='collapse' data-target='#demo"+i+"'><i class='fas fa-angle-down'></i></button>"
            acco+= "<div id='demo"+i+"' class='collapse wrapword text-secondary'>"+trimAppDescription(data[i].description)+"</div></div></div>"
            $("#dynamicAnalyse").append(acco)
    }
}
function analyseForm(appID,appName){
    $.ajax({
        method: "GET",
        dataType: "json",
        async: "true",
		url: location.origin + '/getHtmlDataByAppId/?AppId='+appID,
	    success: function (data, status, jqXHR) {
	        formUI(data.appelements,appName,appID)
            loadingDisplay(".rightLoad",false);
        },
        error: function (jqXHR, status, err) {
             alert("Sorry! Unable to generate Analysis form. Please try again or contact admin.")
	    },
            complete: function (jqXHR, status) {
        }
	});
}
function formUI(data,appName,appID){
    analyseData=data;
    $("#appForm").data("appID",appID);
    $("#analysis").css("display","none");
    $("#appForm").children(".container").children("h5").html("")
    $("#appForm").children(".container").children("h5").append(appName)
    $("#theForm").html("")
    // append the name div for the app
    Analysis_namediv = "<div class='list-group-item'><label for='formGroupExampleInput'>Analysis Name<i class='fas fa-question-circle' href='#' data-toggle='popover' title='Name for the Analysis'></i></label><input type='text' class='form-control' id='Analysisname' placeholder='Analysis Name' value=''></div>"
    $("#theForm").append(Analysis_namediv)
    for(var i=0;i<data.length;i++){
        $("#theForm").append(data[i].html)
        if(data[i].type=="TextSelection"){
            var choice = ""
            $("#"+data[i].id).children(".dropdown").children(".dropdown-menu").children("button").click(function(data){
                choice = $(this).text()
                $(this).parent().parent().children("button").html(choice)
                $(this).parent().parent().children("button").val(choice)
            });

        }
    }
    $("#theForm").append("<div class='list-group-item'><button type='button' class='btn btn-success  w-100' onclick='formRunData()'>Run Analysis</button></div>")
    $("#appForm").css("display","block");
    $("#primaryinput").val($(".right").data("fileInfo").label)
    var baiLabel = null
    $("#theForm").find("label").each(function(){
        if($(this).html().includes(".bai")){
            baiLabel = $(this)
        }
    });
    var currentFiles = $("#fileFolder").data("currentFiles")
    if(baiLabel != null){
        var baiInput =  baiLabel.siblings("input")
        for(i in currentFiles ){
            if(currentFiles[i] == $(".right").data("fileInfo").label.split(".bam")[0]+".bai"){
                baiInput.val(currentFiles[i])
            }else if(currentFiles[i] == $(".right").data("fileInfo").label+".bai" ){
            baiInput.val(currentFiles[i])
            }
        }
    }
   $("#theForm").find("input:text").each(function(){
        $(this).change(function(){
            if($(this).val() == ""){
                $(this).addClass("is-invalid")
            }
        })
    });
}
function formRunData(){
    var fd = new FormData();
    fd.append('AppId',$('#appForm').data("appID"))
    fd.append('outputdir',fileDir)
    var element_fd=new FormData();
    var status=0;
    for(var i=0;i<analyseData.length;i++){
        $("#"+analyseData[i].id).children("input:text").removeClass("is-invalid");
        $("#"+analyseData[i].id).children("dropdownMenu").removeClass("is-invalid");
        var form_element_value = ""
        var splitted = $(".right").data("fileInfo").path.split("/")
        var folderPath = splitted.slice(0,splitted.length-1).join("/")
        switch(analyseData[i].type) {
            case "TextSelection":
                    form_element_value=$("#"+analyseData[i].id).children(".dropdown").children("button").text();
                    if(form_element_value==""){$("#"+analyseData[i].id).children(".dropdown").children("button");status=1;$('.analysisInputError').toast('show');break;}
                break;
            case "FileInput":
                    form_element_value= $("#"+analyseData[i].id).children("input:text").val()
                    if(form_element_value==""){$("#"+analyseData[i].id).children("input:text").addClass("is-invalid");status=1;$('.analysisInputError').toast('show');break;}
                    form_element_value= folderPath+"/"+form_element_value
                break;
            case "FileOutput":
                    form_element_value= $("#"+analyseData[i].id).children("input:text").val()
                    if(form_element_value==""){ $("#"+analyseData[i].id).children("input:text").addClass("is-invalid");status=1;$('.analysisInputError').toast('show');break;}
                break;
             case "Integer":
                    form_element_value= $("#"+analyseData[i].id).children(":input[type='number']").val()
                    if(form_element_value==""){$("#"+analyseData[i].id).children(":input[type='number']").addClass("is-invalid");status=1;$('.analysisInputError').toast('show');break;}
                break;
            case "FolderOutput":
                    form_element_value= $("#"+analyseData[i].id).children("input:text").val()
                    if(form_element_value==""){ $("#"+analyseData[i].id).children("input:text").addClass("is-invalid");status=1;$('.analysisInputError').toast('show');break;}
                break;
            default:

        }
        element_fd.append(analyseData[i].id,form_element_value);
    }
    if(status==0){
        runAnalysis(fd,element_fd)
    }
}
function getInputPath(path){
    path = path.substring(0,path.lastIndexOf("/")+1);
    return path;
}
function runAnalysis(fd,element_fd){
    currentsection = $("#page-selection").data("currentSection")
    $(".analyseToast").toast("show");
    var object = {};
    var element_obj={};
    element_fd.forEach(function(value,key){
        element_obj[key]=value;
    });
    fd.forEach(function(value, key){
        object[key] = value;
    });
    object["config"]=element_obj
    object["currentsection"] = currentsection
    object['redirect_savedurl'] = location.hash.split('#')[1]
    object['Analysisname'] = $('#Analysisname').val();
    var json = JSON.stringify(object);
    $.ajax({
	    url: location.origin+'/runCoverageGraphAnalysis/',
        data: json,
        processData: false,
        contentType: false,
        type: 'POST',
        success: function(data){
	        $('.runAnalysisToast').toast('show');
	        $('.analyseToast').toast('hide');

            closerp();
        },
        error: function (jqXHR, status, err) {
	        if (jqXHR.status == 401){
		        signout(true)
	        }else if(jqXHR.status == 400) {
                window.location.replace("https://auth.cyverse.org/cas5/logout?service=https://connect.bioviz.org/")
            }else if(jqXHR.status == 403){
                $('.analysisError').toast('show');
            }
	    },
            complete: function (jqXHR, status) {
        }
    });
}

function backtoMenu(){
 $("#appForm").css("display","none");
 $("#analysis").css("display","block");
}




