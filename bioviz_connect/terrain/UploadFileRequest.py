import requests
from .. import settings
from ..terrain_model.RequestCreator import RequestCreator
from ..terrain_model.ResponseParser import ResponseParser
from ..terrain_model.CustomEncoder import CustomEncoder


def upload_File(file, filename, dest, redis_sessiondata):
    try:
        accesstoken = str(redis_sessiondata[0])
        files = {'file': (filename, file)}
        req_url = 'https://de.cyverse.org/terrain/secured/fileio/upload?dest='+dest
        r = requests.post(req_url, headers={'Accept': 'application/json',
                                        'Authorization': 'BEARER ' + accesstoken},
                      files=files)

        json_data = r.json()
        if 'error_code' in json_data:
            if (json_data['error_code'] == 'ERR_EXISTS'):
                raise FileExistsError()
        return json_data
    except PermissionError as error:
        raise PermissionError()
    except FileExistsError as error:
        raise FileExistsError()
    except Exception as error:
        print('error is', error)


