import requests
from .. import settings
from ..terrain_model.RequestCreator import RequestCreator
from ..terrain_model.ResponseParser import ResponseParser
from ..terrain_model.CustomEncoder import CustomEncoder

def move_FileFolder(source, dest, redis_sessiondata):
    try:
        accesstoken = str(redis_sessiondata[0])
        request = RequestCreator.moveFileFolder_request(source, dest)
        req_url = 'https://de.cyverse.org/terrain/secured/filesystem/move'
        r = requests.post(req_url, headers={'Accept': 'application/json','Authorization': 'BEARER ' + accesstoken,'Content-type': 'text/plain'},data=request)
        json_data = r.json()
        if 'error_code' in json_data:
            if (json_data['error_code'] == 'ERR_EXISTS'):
                raise FileExistsError()
            if (json_data['error_code'] == 'ERR_DOES_NOT_EXIST'):
                raise FileNotFoundError()
            if (json_data['error_code'] == 'ERR_TOO_MANY_PATHS'):
                raise BufferError()
        return json_data
    except FileExistsError as error:
        raise FileExistsError()
    except FileNotFoundError as error:
        raise FileNotFoundError()
    except BufferError as error:
        return BufferError()
    except Exception as error:
        return Exception()
