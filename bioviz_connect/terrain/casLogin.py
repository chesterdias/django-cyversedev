#https://auth.cyverse.org/cas5/oauth2.0/authorize?response_type=code&client_id=l3wrKsrCJKbBdZ9kYfNE/Kr7scndD4nV&redirect_uri=https://pawancyverse.bioviz.org/
from .. import settings


def casLogin():
    response = settings.AUTH_URL + "?" + "response_type=code&client_id=" + settings.CLIENT_ID + "&redirect_uri=" + settings.REDIRECT_URI
    return response


