# unshare file
import requests
from .. import settings
from ..terrain_model.ResponseParser import ResponseParser
from ..terrain_model.RequestCreator import RequestCreator
from ..terrain_model.CustomEncoder import CustomEncoder


def unshareFile(filepath, user, accesstoken):
    try:
        request_data = RequestCreator.create_unshare_request(filepath, user)
        req_url = 'https://de.cyverse.org/terrain/secured/unshare'
        r = requests.post(req_url, headers={'Authorization': 'BEARER ' + accesstoken,
                                       'Content-type': 'application/json'},
                      data=request_data)

        json_data = r.json()
        if 'error_code' in json_data:
            if (json_data['error_code'] == 'ERR_NOT_AUTHORIZED'):
                raise PermissionError()

        response = ResponseParser.unshare_response(r.json())
        custom_encoder = CustomEncoder()
        res = custom_encoder.encode(response)
        return res
    except PermissionError as error:
        raise PermissionError()

