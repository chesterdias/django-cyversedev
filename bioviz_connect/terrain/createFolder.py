import requests
from .. import settings
from ..terrain_model.RequestCreator import RequestCreator
from ..terrain_model.ResponseParser import ResponseParser
from ..terrain_model.CustomEncoder import CustomEncoder


def createDirectory(filepath, redis_sessiondata):
    try:
        # Multiple filepaths can be send separated by ','
        accesstoken = str(redis_sessiondata[0])
        request = RequestCreator.createFolderRequest(filepath)
        req_url = 'https://de.cyverse.org/terrain/secured/filesystem/directory/create'
        r = requests.post(req_url, headers={'Authorization': 'BEARER ' + accesstoken,
                                        'Content-type': 'application/json'},
                      data=request)

        json_data = r.json()
        if 'error_code' in json_data:
            if (json_data['error_code'] == 'ERR_EXISTS'):
                raise FileExistsError()

        #response = ResponseParser.parse_file_permission_response(r.json())
        return json_data
    except PermissionError as error:
        raise PermissionError()


