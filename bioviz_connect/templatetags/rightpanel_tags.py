from django import template

register = template.Library()

@register.inclusion_tag('rightpanel.html')
def rightpanel_render():
    return


@register.inclusion_tag('rp_metadata.html')
def metadata_render():
    return

@register.inclusion_tag('rp_managelink.html')
def managelink_render():
    return


@register.inclusion_tag('rp_analysis.html')
def analysis_render():
    return

